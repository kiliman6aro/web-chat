<?php
define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', dirname(__FILE__));

require_once(dirname(__FILE__).DS.'app'.DS.'controllers'.DS.'ChatController.php');

$controller = new ChatController();

switch ($_GET['action']) {
    case 'send':
        $controller->actionSend();
        break;
    case 'messages':
        $controller->actionAllMessages();
        break;
    default:
         $controller->actionIndex();
        break;
}

function __autoload($class){
    $file = dirname(__FILE__).DS.'app'.DS.'common'.DS.$class.'.php';
    if(file_exists($file)){
        require_once($file);
    }
}