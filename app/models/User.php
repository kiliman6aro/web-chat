<?php
/**
 * User: Bilyk Pavel
 * Date: 17.01.2016
 */

namespace app\models;


class User extends BaseModel{

    public $username;

    public $email;

    public $password;

    public $url;

    public static function model($attributes){
        $user = new User();
        $user->username = strtolower($attributes['username']);
        $user->email = $attributes['email'];
        $user->password = md5($attributes['password']);
        $user->url = isset($attributes['url']) ? $attributes['url'] : null;
        return $user;
    }

    public function validate($action = null){
        if(empty($this->username)){
            $this->setError("username", "Поле не может быть пустым");
        }
        if(empty($this->password)){
            $this->setError("password", "Поле не может быть пустым");
        }
        if($action == 'registration'){
            if(!filter_var($this->email,FILTER_VALIDATE_EMAIL)){
                $this->setError("email", "Не корректный Email");
            }
            if(!empty($this->url) && !filter_var($this->url, FILTER_VALIDATE_URL)){
                $this->setError("url", "Не корректный URL");
            }
        }
        return $this->hasErrors();
    }
}