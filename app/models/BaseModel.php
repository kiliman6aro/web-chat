<?php
/**
 * Created by PhpStorm.
 * User: kilim
 * Date: 19.01.2016
 * Time: 11:01
 */

namespace app\models;


abstract class BaseModel{

    public $id;

    private $_errors = array();

    abstract public function validate();

    public function setError($field, $msg){
        $this->_errors[$field] = $msg;
    }
    public function hasErrors(){
        return empty($this->_errors);
    }

    public function getErrors(){
        return $this->_errors;
    }

}