<?php
/**
 * Created by PhpStorm.
 * User: kilim
 * Date: 19.01.2016
 * Time: 13:30
 */

namespace app\models;


class Message extends BaseModel{

    const TYPE_MESSAGE = 'message';

    const TYPE_ATTACH = 'attach';

    public $body;

    public $type;

    public $from;

    public $time = 0;

    public static function model($attributes){
        $model = new Message();
        $model->body = $attributes['body'];
        $model->from = isset($attributes['from']) ? $attributes['from'] : null;
        $model->type = isset($attributes['type']) ? $attributes['type'] : self::TYPE_MESSAGE;
        $model->time = time();
        return $model;
    }

    public function __construct($from = null, $body = null, $type = self::TYPE_MESSAGE, $time = null){
        $this->from = $from;
        $this->body = $body;
        $this->type = $type;
        $this->time = ($time == null) ? time() : $time;
    }

    public function validate(){
        if(empty($this->body)){
            $this->setError("body", "Поле не может быть пустым");
        }
        if(empty($this->from)){
            $this->setError("from", "Поле не может быть пустым");
        }
        if((int)$this->time == 0){
            $this->setError("time", "Время должно быть указана в формате unixtime");
        }
        if($this->type != Message::TYPE_ATTACH && $this->type != Message::TYPE_MESSAGE){
            $this->setError("time", "Неизвестный тип сообщения");
        }
        return $this->hasErrors();
    }


}