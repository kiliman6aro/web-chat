<?php

abstract class BaseController{
    
    protected $defaultLayout = 'main';
    
    public function render($file, $params = array(), $output = false){
        if(file_exists($this->getViewsPath().DS.$file.'.php')){
            extract($params);
            if($output){
                include($this->getViewsPath().DS.$file.'.php');
            }else{
                ob_start();
                include($this->getViewsPath().DS.$file.'.php');
                $result = ob_get_contents(); 
                ob_end_clean();
                return $result;
            }
        }            
    }
    public function getViewsPath(){
        return BASE_PATH.DS.'app'.DS.'views';
    }
}