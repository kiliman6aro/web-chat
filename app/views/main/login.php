<div class="modal bs-modal-sm" data-keyboard="false" data-backdrop="static" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <br>
            <div class="bs-example bs-example-tabs">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active"><a href="#signin" data-toggle="tab">Вход</a></li>
                    <li class=""><a href="#signup" data-toggle="tab">Регистрация</a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="signin">
                        <form class="form-horizontal" id="form-signin" method="post">
                            <fieldset>
                                <!-- Sign In Form -->
                                <!-- Text input-->
                                <div class="control-group">
                                    <label class="control-label" for="username">Имя пользователя:</label>
                                    <div class="controls">
                                        <input id="username" name="username" type="text" class="form-control" placeholder="Имя пользователя" class="input-medium" required>
                                    </div>
                                </div>

                                <!-- Password input-->
                                <div class="control-group">
                                    <label class="control-label" for="password">Пароль:</label>
                                    <div class="controls">
                                        <input id="password" name="password" class="form-control" type="password" placeholder="********" class="input-medium" required>
                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="control-group">
                                    <label class="control-label" for="signin"></label>
                                    <div class="controls">
                                        <button id="signin" name="signin" class="btn btn-success">Войти</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="signup">
                        <form class="form-horizontal" id="form-signup">
                            <fieldset>
                                <!-- Sign Up Form -->
                                <!-- Text input-->
                                <div class="control-group">
                                    <label class="control-label" for="email">Email:</label>
                                    <div class="controls">
                                        <input id="email" name="email" class="form-control" type="text" placeholder="username@example.com" class="input-large" required>
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="control-group">
                                    <label class="control-label" for="username">Имя пользователя:</label>
                                    <div class="controls">
                                        <input id="username" name="username" type="text" class="form-control" placeholder="Имя пользователя" class="input-medium" required>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="url">URL:</label>
                                    <div class="controls">
                                        <input id="url" name="url" type="text" class="form-control" placeholder="http://" class="input-medium">
                                    </div>
                                </div>

                                <!-- Password input-->
                                <div class="control-group">
                                    <label class="control-label" for="password">Пароль:</label>
                                    <div class="controls">
                                        <input id="password" name="password" class="form-control" type="password" placeholder="********" class="input-medium" required>
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="control-group">
                                    <label class="control-label" for="password2">Пароль еще раз:</label>
                                    <div class="controls">
                                        <input id="password2" name="password2" class="form-control" type="password" placeholder="********" class="input-medium" required>
                                    </div>
                                </div>
                                <!-- Button -->
                                <div class="control-group">
                                    <label class="control-label" for="confirmsignup"></label>
                                    <div class="controls">
                                        <button id="confirmsignup" name="confirmsignup" class="btn btn-success">Зарегестрироваться</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>