<?php if($username != $message->from): ?>
<li class="left clearfix">
    <span class="chat-img pull-left">
        <img src="http://placehold.it/50/55C1E7/fff&text=<?php echo htmlspecialchars(ucfirst($message->from)[0]) ?>" alt="User Avatar" class="img-circle" />
    </span>
    <div class="chat-body clearfix">
        <div class="header">
            <strong class="primary-font"><?php echo ucfirst(htmlspecialchars($message->from)) ?></strong>
            <small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span><?php echo date("Y-m-d H:i:s", $message->time) ?></small>
        </div>
        <?php if($message->type == \app\models\Message::TYPE_MESSAGE): ?>
            <p>
                <?php echo htmlspecialchars($message->body); ?>
            </p>
        <?php else: ?>
            <?php echo $this->render('_attached', array('message' => $message)); ?>
        <?php endif; ?>
    </div>
</li>
<?php else: ?>
<li class="right clearfix">
    <span class="chat-img pull-right">
            <img src="http://placehold.it/50/FA6F57/fff&text=Я" alt="User Avatar" class="img-circle" />
    </span>
    <div class="chat-body clearfix">
        <div class="header">
            <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?php echo date("Y-m-d H:i:s", $message->time) ?></small>
            <strong class="pull-right primary-font"><?php echo ucfirst(htmlspecialchars($message->from))  ?></strong>
        </div>
        <?php if($message->type == \app\models\Message::TYPE_MESSAGE): ?>
        <p>
            <?php echo htmlspecialchars($message->body); ?>
        </p>
        <?php else: ?>
        <?php echo $this->render('_attached', array('message' => $message)); ?>
        <?php endif; ?>
    </div>
</li>
<?php endif; ?>


