<div id="panel-primary" class="panel panel-primary">
    <div class="panel-body">
        <ul class="chat">
            <?php
                if(isset($messages)){
                    foreach($messages as $msg){
                        echo $msg."\n";
                    }
                }
            ?>
        </ul>
    </div>
</div>
<div class="panel-footer">
    <div class="input-group">
        <input id="chat-input" type="text" class="form-control input-sm" placeholder="Введите текст сообщения..."/>
            <span class="input-group-btn">
                <button class="btn btn-warning btn-sm" id="btn-chat">Сказать</button>
            </span>
    </div>
</div>
<?php if($username): ?>
<script type="text/javascript">
    var user = <?php echo json_encode($username); ?>;
</script>
<?php endif; ?>
