<li class="right clearfix">
    <span class="chat-img pull-right">
        <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
    </span>
    <div class="chat-body clearfix">
        <div class="header">
            <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>15 mins ago</small>
            <strong class="pull-right primary-font"><?php echo $username ?></strong>
        </div>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales.
        </p>
    </div>
</li>