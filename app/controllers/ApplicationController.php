<?php
/**
 * Created by PhpStorm.
 * User: kilim
 * Date: 19.01.2016
 * Time: 10:37
 */

namespace app\controllers;


use system\web\BaseController;

class ApplicationController extends BaseController{

    const RESPONSE_OK = 'OK';

    const RESPONSE_ERROR = 'ERROR';

    const ERROR_CODE_BAD_REQUEST = 400; //Не прошёл валидацию

    const ERROR_CODE_FORBIDDEN = 403;//Не авторизован, потеря сессии

    const ERROR_CODE_SERVER_ERROR = 500; //Не известная ошибка, не удалось сохранить или получить данные

    /**
     * Возвращает jSON ответ. Если ответ, сообщает об ошибке
     * то передается код ошибки. В зависимости от кода ошибки,
     * клиент определяет какой тип объекта лежит в response.
     * @param $response объект или массив
     * @param string $status состояние ответа
     * @param null $codeError код ошибки
     */
    protected function responseAJAX($response, $status = self::RESPONSE_OK, $codeError = null){
        $output = array('status' => $status, 'response' => $response);
        if($status == self::RESPONSE_ERROR){
            $output['code'] = $codeError ? $codeError : self::ERROR_CODE_SERVER_ERROR;
        }
        echo json_encode($output);
        exit;
    }
}