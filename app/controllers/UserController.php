<?php
/**
 * User: Bilyk Pavel
 * Date: 17.01.2016
 */

namespace app\controllers;


use app\components\mappers\UsersMapper;
use app\models\User;
use system\core\Registry;

class UserController extends  ApplicationController{

    private $_mapper;

    public function init(){
        $this->_mapper = Registry::get('users');
    }

    public function actionRegistration(){
        if($this->_mapper->findByAttributes(array('username' => strtolower($_POST['username'])))){
            $this->responseAJAX(array('username' => 'Такой пользователь существует'), self::RESPONSE_ERROR, self::ERROR_CODE_BAD_REQUEST);
        }
        $user = User::model($_POST);
        if(!$user->validate('registration')){
            $this->responseAJAX($user->errors, self::RESPONSE_ERROR, self::ERROR_CODE_BAD_REQUEST);
        }
        if($user = $this->_mapper->insert($user)){
            $this->_login($user);
            $this->responseAJAX($user);
        }
        $this->responseAJAX("Не удалость сохранить пользователя в базу данных", self::RESPONSE_ERROR);
    }

    public function actionLogin(){
        $user = $this->_mapper->findByAttributes(array('username' => strtolower($_POST['username']), 'password' => md5($_POST['password'])));
        if(!$user){
            $this->responseAJAX(array('password' => 'Не правильный логин или пароль'), self::RESPONSE_ERROR, self::ERROR_CODE_BAD_REQUEST);
        }
        $this->_login($user);
        $this->responseAJAX($user);
    }

    public function actionTest(){
        $user = $this->_mapper->findByAttributes(array('username' => strtolower($_POST['username']), 'password' => md5($_POST['password'])));
        echo '<pre>';
        print_r($user);

    }
    /**
     * Вызывается во время клиентской валидации, для проверки
     * существования такого пользователя.
     */
    public function actionCheckUsername(){
        $username = strtolower($_POST['username']);
        if($this->_mapper->findByAttributes(array('username' => $username))){
            echo 'false';
            exit;
        }
        echo 'true';
        exit;
    }

    private function _login($user){
        Registry::get('session')->set('username', $user->username);

        if(isset($_POST['timezone'])){
            Registry::get('session')->set('timezone', $_POST['timezone']);
        }
    }
}