<?php

class ChatController extends BaseController{
    
    public function actionIndex(){
        $this->render('index', array('user' => array('username' => 'Kilimangaro')), true);
    }
    
    public function actionSend(){
        echo $this->render('message', array('username' => 'kilimangaro'));
    }
    
    public function actionAllMessages(){
        echo 'messages';
    }
}