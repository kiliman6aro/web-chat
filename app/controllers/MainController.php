<?php

namespace app\controllers;

use app\components\mappers\MessagesMapper;
use app\models\Message;
use system\core\Registry;

class MainController extends ApplicationController{

    public function init(){
        if(isset($_SESSION['timezone'])){
            date_default_timezone_set($_SESSION['timezone']);
        }
        if(isset($_SESSION['username'])){
            $this->username = $_SESSION['username'];
        }
    }

    /**
     * Если пользователь авторизован, то получить все сообщения
     * записать временную метку, последнего получения сообщений
     * Передать имя пользователя в вид.
     * @throws \Exception
     */
    public function actionIndex(){
        $variables = array();
        if(!Registry::get('session')->isGuest()){
            $variables['username'] = Registry::get('session')->get('username');
            $models = Registry::get('messages')->findAll();
            $variables['messages'] = array_map(array($this, '_renderMessage'), $models);
            Registry::get('session')->set(MessagesMapper::FIELD_LAST_REQUEST_MESSAGES, time());
        }
        $this->display('index', $variables);
    }

    /**
     * Проверить наличие новых сообщений.
     * Возвращает сообщения, которые были добавлены позже временной
     * метки и не пренадлежат текущему пользователю.
     */
    public function actionMessages(){
        if(Registry::get('session')->isGuest()){
            $this->responseAJAX("Необходима авторизация", self::RESPONSE_ERROR, self::ERROR_CODE_FORBIDDEN);
        }
        $response = array();
        $models =  Registry::get('messages')
                    ->findNewMessages(Registry::get('session')
                        ->get(MessagesMapper::FIELD_LAST_REQUEST_MESSAGES, 0));
        if(count($models) > 0){
            $response['messages'] = array_map(array($this, '_renderMessage'), $models);
            Registry::get('session')->set(MessagesMapper::FIELD_LAST_REQUEST_MESSAGES, time());
        }
        $response['count'] = count($models);
        $this->responseAJAX($response);
    }

    /**
     * Вернуть всю коллекцию сообщений
     */
    public function actionGetAllMessages(){

    }
    public function actionSend(){
        if(Registry::get('session')->isGuest()){
            $this->responseAJAX("Необходима авторизация", self::RESPONSE_ERROR, self::ERROR_CODE_FORBIDDEN);
        }
        $msg = Message::model($_POST);
        $msg->from = $this->username;
        $msg = Registry::get('messages')->insert($msg);
        $html = $this->_renderMessage($msg);
        Registry::get('session')->set(MessagesMapper::FIELD_LAST_REQUEST_MESSAGES, time());
        $this->responseAJAX(array('message' => $msg, 'html' => $html));
    }

    public function actionUpload(){
        foreach($_FILES as $file){
            if($file['type'] != 'text/plain'){
                $this->responseAJAX("Формат файла должен быть *.txt", self::RESPONSE_ERROR);
            }
            if($file['size'] > 100000){
                $this->responseAJAX("Размер файла не должен быть больше 100кб", self::RESPONSE_ERROR);
            }
            $name = uniqid();
            if(copy($file['tmp_name'], BASE_PATH.DS.'public'.DS.'upload'.DS."$name.txt")){
                $msg = new Message();
                $msg->body = $name;
                $msg->from = $this->username;
                $msg->type = Message::TYPE_ATTACH;
                if($msg = Registry::get('messages')->insert($msg)){
                    $html = $this->_renderMessage($msg);
                    Registry::get('session')->set(MessagesMapper::FIELD_LAST_REQUEST_MESSAGES, time());
                    $this->responseAJAX(array('message' => $msg, 'html' => $html));
                }
            }
            $this->responseAJAX("Не удалось сохранить файл", self::RESPONSE_ERROR);
        }
    }

    public function actionFile($id){
        header("Content-Type: text/plain");
        $msg = Registry::get('messages')->findById($id);
        if(!$msg || $msg->type != Message::TYPE_ATTACH){
            echo 'Файл не найден';
            exit;
        }
        $file = BASE_PATH.DS.'public'.DS.'upload'.DS."$msg->body.txt";
        echo htmlspecialchars(file_get_contents($file));
        exit;
    }

    /**
     * Генерирует HTML сообщения
     * @param $msg
     * @return string
     * @throws \Exception
     */
    private function _renderMessage($msg){
        return $this->render('message', array('message' => $msg, 'username' => $this->username));
    }
}