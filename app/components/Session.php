<?php

namespace app\components;


class Session implements \IComponent{

    public function init(){
        session_start();
        if(isset($_SESSION['timezone'])){
            date_default_timezone_set($_SESSION['timezone']);
        }
    }

    public function set($name, $value){
        $_SESSION[$name] = $value;
    }

    public function get($name, $defaultValue = null){
        return isset($_SESSION[$name]) ? $_SESSION[$name] : $defaultValue;
    }

    public function isGuest(){
        if($this->get('username') != null)
            return  false;

        return true;
    }

}