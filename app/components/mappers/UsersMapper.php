<?php
/**
 * User: Bilyk Pavel
 * Date: 17.01.2016
 */

namespace app\components\mappers;


use app\models\User;

class UsersMapper extends FileMapper{

    public function createObject($data) {
        $user = new User();
        $user->id = $data['id'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->password = $data['password'];
        $user->url = $data['url'];
        return $user;
    }
}