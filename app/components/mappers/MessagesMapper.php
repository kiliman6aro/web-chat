<?php
/**
 * Created by PhpStorm.
 * User: kilim
 * Date: 19.01.2016
 * Time: 13:29
 */

namespace app\components\mappers;


use app\models\Message;

class MessagesMapper extends FileMapper{

    const FIELD_LAST_REQUEST_MESSAGES = "msgtime";

    public function createObject($data){
        $model = new Message();
        $model->type = $data['type'];
        $model->body = $data['body'];
        $model->from = $data['from'];
        $model->time = $data['time'];
        $model->id = $data['id'];
        return $model;
    }

    /**
     * Возвращает последние сообщения
     * @param $time временная метка, позже которой необходимы сообщения
     * @return array сообщения
     */
    public function findNewMessages($time){
        $msgs = $this->findAll();
        $response = array();
        foreach($msgs as $msg){
            if($msg->time > $time){
                $response[] = $msg;
            }
        }
        return $response;
    }

}