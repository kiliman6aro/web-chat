<?php
/**
 * User: Bilyk Pavel
 * Date: 17.01.2016
 */

namespace app\components\mappers;


use app\models\BaseModel;

abstract class BaseMapper implements \IComponent{

    protected $items = array();

    abstract public function insert(BaseModel $model);

    abstract public function update(BaseModel $model);

    abstract public function delete(BaseModel $model);

    abstract public function findById($id);

    abstract public function findByAttributes($attributes);

    abstract public function findAllByAttributes($attributes);

    abstract public function findAll();
}