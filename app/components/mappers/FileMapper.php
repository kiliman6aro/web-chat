<?php
/**
 * User: Bilyk Pavel
 * Date: 17.01.2016
 */

namespace app\components\mappers;



use app\models\BaseModel;

abstract class FileMapper extends BaseMapper{

    public $file;


    abstract public function createObject($data);


    public function init(){
        if(!file_exists($this->file)){
            throw new \Exception("Не найден файл с данными ".$this->file);
        }
    }

    /**
     * Добавляет объект в коллекцию.
     * @param BaseModel $model
     * @return BaseModel
     * @throws \Exception
     */
    public function insert(BaseModel $model) {
        $this->_read();
        $model->id = $this->_generateId($this->items);
        $this->items[] = $model;
        $this->_save();
        return $model;
    }

    /**
     * Осуществляет поиск элемента по ID из переданной модели
     * и если он найден, замещает его.
     * @param BaseModel $model
     * @return bool|int
     * @throws \Exception
     */
    public function update(BaseModel $model){
        $items = $this->_search(array('id' => $model->id), function($index, $item) use($model){
            $this->items[$index] = $model;
        });
        if(count($items) > 0){
            $this->_save();
            return true;
        }
        return false;
    }

    /**
     * Осуществляет поиск элемента по ID из переданной модели
     * и удаляет найденный элемент.
     * @param BaseModel $model
     * @return bool
     * @throws \Exception
     */
    public function delete(BaseModel $model) {
        $items = $this->_search(array('id' => $model->id), function($index, $item){
            unset($this->items[$index]);
        });
        $this->items = array_values($this->items);
        $this->_save();
        return count($items) > 0 ? true : false;
    }

    /**
     * Осщуествляет поиск по атрибуту ID. В действительности
     * синоним функции FileMapper::findByAttributes.
     * @param $id
     * @return array
     */
    public function findById($id) {
        return $this->findByAttributes(array('id' => $id));
    }

    /**
     * TODO: реализовать через callback функцию, что бы возврат был, по первому найденному
     * Осуществляет поиск элементов, по атрибутам и возвращает
     * первый из них
     * @param $attributes набор атрибутов для отбора
     * @return array
     */
    public function findByAttributes($attributes) {
        $result = $this->_search($attributes);
        if(count($result) > 0){
            return $result[0];
        }
        return null;
    }

    /**
     * Осуществляет поиск элементов, по атрибутам и вовзвращает
     * все полученные элементы
     * @param $attributes
     * @return array
     */
    public function findAllByAttributes($attributes) {
        return $this->_search($attributes);
    }

    /**
     * Возвращают всю коллекцию элементов. Если $grab = false,
     * то не выполняет подгрузку из файла, а возвращает то, что
     * уже находится в локальной коллекции.
     * @param bool|true $grab
     * @return array
     */
    public function findAll($grab = true){
        if($grab == false && !empty($this->items)){
            return $this->items;
        }
        return $this->_read();
    }

    /**
     * Читает даные из файла, декодирует данные в JSON, генерирует
     * сущености и наполняет массив
     * @return array|mixed массив сущностей
     */
    private function _read(){
        $this->items = array();
        $result = json_decode(file_get_contents($this->file), true);
        if(is_array($result) && !empty($result)){
            foreach($result as $item){
                $this->items[] = $this->createObject($item);
            }
            return $this->items;
        }
        return array();
    }

    /**
     * Кодирует данные из коллекции в JSON и сохраняет их в файл.
     * @throws \Exception
     */
    private function _save(){
        $source = json_encode($this->items);
        $fp = fopen($this->file, "r+");
        if(flock($fp, LOCK_EX)){
            ftruncate($fp, 0);
            fwrite($fp, $source);
            fflush($fp);
            flock($fp, LOCK_UN);
        }else{
            throw new \Exception("Файл занят другим процессом");
        }
        $this->items = array();
        fclose($fp);
    }

    /**
     * Получает все данные из файла, в виде массива, а затем
     * фильтрует его исходя из переданных атрибутов. Если,
     * атрибутов переданно несколько, то вернет только те,
     * элементы, у которых полное совпадение по атрибутам.
     *
     * Если передана $callback функция, то она будет вызвана и в неё
     * будет передан элемент и его index.
     *
     * @param $attributes массив атрибутов
     * @param string $callback
     * @return array элементы попавшие под условия выборки
     */
    private function _search($attributes, $callback = ''){
        $this->_read();
        $data = array();
        foreach($this->items as $index => $item){
            $result = false;
            foreach ($attributes as $key => $value) {
                if(isset($item->$key) && $item->$key != $value){
                    break;
                }
                $result = true;
            }
            if($result == false)
                continue;

            if(!empty($callback))
                $callback($index, $item);

            $data[] = $item;
        }
        return $data;
    }

    /**
     * Генерирует идентификатор для сущностей. Переберает существующие
     * сущности, выбирает наибольший по значению и возвращает, на еденицу
     * больше самого большого. Если элементов нет, то возвращает еденицу.
     * @param array $items
     * @return int
     */
    private function _generateId($items = array()){
        $id = 0;
        if(count($items) == 0){
            return 1;
        }
        foreach($items as $item){
            if($item->id <= $id)
                continue;

            $id = $item->id;
        }
        return $id + 1;
    }

}