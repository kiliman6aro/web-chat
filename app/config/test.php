<?php
return array(
    'components' => array(
        'users' => array(
            'file' => dirname(__FILE__).DS.'..'.DS.'data'.DS.'test-users.json',
            'class' => "\\app\\components\\mappers\\UsersMapper"
        ),
        'messages' => array(
            'file' => dirname(__FILE__).DS.'..'.DS.'data'.DS.'channels'.DS.'test-general.json',
            'class' => "\\app\\components\\mappers\\MessagesMapper"
        ),
        'session' => array(
            'class' => "\\app\\components\\Session"
        ),
    )
);