var chat = (function(){
    function Chat(){
        var self = this;

        var uploadArea = upload ? upload : {};

        var chatContainer = $('.chat');

        var inputMessage  = $('#chat-input');

        var btnChat = $('#btn-chat');

        var interval = null;
        var loginWindow = $('#signupModal');

        var messages = [];

        var INTERVAL_PENDING = 2000;

        var ACTION = {
            PENDING: "/main/messages",
            SEND:'/main/send',
            UPLOAD: "/main/upload",
            ATTACH:"/main/file/"
        }

        if(window.user === undefined || window.user === null){
            loginWindow.modal('show');
        }else{
            this.user = window.user;
            _open();
        }

        this.logout = function(){
            this.user = null;
            loginWindow.modal('show');
            _close();
        }

        /**
         * Отправляет сообщение
         * @param msg текст сообщения
         */
        this.send = function(msg){
            $.ajax({
                type: "POST",
                url: ACTION.SEND,
                data: {
                    "body": msg,
                },
                dataType:'json',
                success:function(data){
                    if(data.status == 'OK'){
                        _appendMessage(data.response.html);
                        inputMessage.val('');
                        return;
                    }
                    //Если проблема с доступом, нужно предложить авторизацию по новой
                    if(data.status == 'ERROR' && data.code == 403){
                        chat.logout();
                        return;
                    }
                    throw new Error(data.response);
                }
            });
        }

        /**
         * Отправляет файл
         * @param file объект File
         */
        this.attach = function(file){
            var formData = new FormData();
            formData.append(file.name, file);
            $.ajax({
                url: ACTION.UPLOAD,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType:'json',
                success: function(data){
                    if(data.status == 'OK'){
                        _appendMessage(data.response.html);
                        return;
                    }
                    //Если проблема с доступом, нужно предложить авторизацию по новой
                    if(data.status == 'ERROR' && data.code == 403){
                        chat.logout();
                        return;
                    }
                    throw new Error(data.response);
                }
            });
        }

        /**
         * Отображает прикрепленный файл в poup окне
         * @param id
         */
        this.showFile = function(id){
            $('#file-show-modal').find('.modal-body').load(ACTION.ATTACH+id, function(){
                $('#file-show-modal').modal('show');
            });
        }

        /**
         * Открывает соединение, задает интервал обращения
         * к серверу за обновлениями.
         * @private
         */
        function _open(){
            $(document).on('keypress',function(event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13' && inputMessage.val() != '') {
                    self.send(inputMessage.val());
                }
            });
            btnChat.on('click', function(){
                if(inputMessage.val() != '') {
                    self.send(inputMessage.val());
                }
            });

            $(uploadArea).on('attach-file', function(e, file){
                self.attach(file);
            });
            interval = setInterval(_pending, INTERVAL_PENDING);
        }

        /**
         * Выполняет запросы к серверу, получает новые сообщения,
         * добавляет их в область чата.
         * @private
         */
        function _pending(){
            $.ajax({
                type: "POST",
                url: ACTION.PENDING,
                dataType:'json',
                success:function(data){
                    if(data.status == 'OK'){
                        for(var i = 0; i < data.response.count; i++){
                            _appendMessage(data.response.messages[i]);
                        }
                        return;
                    }
                    //Если проблема с доступом, нужно предложить авторизацию по новой
                    if(data.status == 'ERROR' && data.code == 403){
                        chat.logout();
                        return;
                    }
                    throw new Error(data.response);
                }
            });
        }

        /**
         * Закрывает соединение с чатом, выполняет
         * выход пользователя, выключает кнопку отправаление
         * сообщений, отображает окно авторизации.
         * @private
         */
        function _close(){
            $(document).unbind('keypress');
            btnChat.unbind('click');
            $(uploadArea).unbind('attach-file');
            clearInterval(interval);
        }

        /**
         * Вставляет в область чата сообщение
         * @param html
         * @private
         */
        function _appendMessage(html){
            chatContainer.append(html);
        }
    }
    return new Chat();
}());

var signInValidator = $('#form-signin').validate({
    rules: {
        username: {
            minlength: 2,
            required: true
        },
        password: {
            required: true,
        }
    },
    //errorClass:'help-block',
    highlight: function (element) {
        console.log(element);
        $(element).closest('.control-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        element.closest('.control-group').removeClass('has-error').addClass('has-success');
        element.remove();
    },
    submitHandler: function (form) {
        var data = $(form).serialize();
        if(jstz !== undefined){
            data+="&timezone="+jstz.determine().name();
        }
        $.ajax({
            type: "POST",
            url: "/user/login",
            data: data,
            dataType:'json',
            success: function (data) {
                if(data.status == "OK"){
                    location.reload();
                }
                if(data.status == "ERROR" && data.code == 400){
                    signInValidator.showErrors(data.response);
                }
            }
        });
        return false;
    }
});
var signUpValidator = $('#form-signup').validate({
    rules: {
        username: {
            minlength: 2,
            required: true,
            remote:"/user/checkusername"
        },
        password: {
            required: true,
        },
        password2: {
            required: true,
            equalTo: $('#form-signup').find('#password'),
        },
        email: {
            required: true,
            email:true
        },
        url:{
            url:true
        }
    },
    messages: {
        password2: {
            equalTo: "Пароли не совпадают",
        }
    },
    //errorClass:'help-block',
    highlight: function (element) {
        $(element).closest('.control-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        element.closest('.control-group').removeClass('has-error').addClass('has-success');
        element.remove();
    },
    submitHandler: function (form) {
        var data = $(form).serialize();
        if(jstz !== undefined){
            data+="&timezone="+jstz.determine().name();
        }
        $.ajax({
            type: "POST",
            url: "/user/registration",
            data: data,
            dataType:'json',
            success: function (data) {
                if(data.status == "OK"){
                    location.reload();
                }
                if(data.status == "ERROR" && data.code == 400){
                    signUpValidator.showErrors(data.response);
                }
            }
        });
        return false;
    }
});
