/**
 * Created by Павел on 17.01.2016.
 */
var upload = (function(){
    function Upload(){
        var self = this;
        var chatArea = $('#panel-primary'),
            maxFileSize = 100000;

        chatArea[0].ondragover = function() {
            if(!chatArea.hasClass('hover')){
                chatArea.addClass('hover');
            }
            return false;
        };

        chatArea[0].ondragleave = function() {
            chatArea.removeClass('hover');
            return false;
        };
        chatArea[0].ondrop = function(event) {
            event.preventDefault();
            chatArea.removeClass('hover');
            chatArea.addClass('drop');

            var file = event.dataTransfer.files[0];

            // Проверяем размер файла
            if (file.size > maxFileSize) {
                alert("Файл не должен привышать 100кб");
                return false;
            }
            if(file.type !== "text/plain"){
                alert("Только файлы *.txt");
                return false;
            }
            $(self).trigger('attach-file', [file]);
        };
    }
    return new Upload();
}());