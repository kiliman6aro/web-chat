<?php
if (version_compare(phpversion(), '5.4', '<')) {
    die("Необходима версия PHP 5.4 и выше");
}
define('DEBUG_MODE', false);
require_once('../system/bootstrap.php');

$config = require_once(dirname(__FILE__).'/../app/config/main.php');

use system\core\Registry;
use system\core\Router;

Registry::load($config);

Router::load($_GET['url']);
