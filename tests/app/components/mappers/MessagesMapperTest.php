<?php
/**
 * User: Bilyk Pavel
 * Date: 19.01.2016
 */

namespace tests\app\components\mappers;


use system\core\Registry;
use app\models\Message;

class MessagesMapperTest extends \PHPUnit_Framework_TestCase {

    private $_mapper;


    protected function setUp(){
        $this->_mapper = Registry::get('messages');
    }

    public function testInsert(){
        $message = new Message();
        $message->body = "Hello, this is first message";
        $message->type = Message::TYPE_MESSAGE;
        $message->from = 'kilimangaro';
        $message->time = time();

        $msg = $this->_mapper->insert($message);
        $this->assertNotNull($msg);

        $model = new Message("qwerty", "Hi! This is second test message!");
        $msg = $this->_mapper->insert($model);
        $this->assertNotNull($msg);

        $messages = $this->_mapper->findAll();
        $this->assertEquals(2, count($messages));
        $this->assertInstanceOf('app\models\Message', $messages[0]);
        $this->assertInstanceOf('app\models\Message', $messages[1]);

        $message = new Message();
        $message->body = "Hello, this is three message";
        $message->type = Message::TYPE_MESSAGE;
        $message->from = 'taras';
        $message->time = (time() + 10);
        print($message->time)."\n";
        $this->_mapper->insert($message);

        $message = new Message();
        $message->body = "Hello, this is four message";
        $message->type = Message::TYPE_MESSAGE;
        $message->from = 'valera';
        $message->time = (time() + 10);
        print($message->time)."\n";
        $this->_mapper->insert($message);

        $message = new Message();
        $message->body = "Hello, this is five message";
        $message->type = Message::TYPE_MESSAGE;
        $message->from = 'qwerty';
        $message->time = (time() + 10);
        $this->_mapper->insert($message);

        $messages = $this->_mapper->findAll();
        $this->assertEquals(5, count($messages));
    }

    public function testFindNewMessages(){
        $now = time();
        print $now."\n";
        $messages = $this->_mapper->findNewMessages($now);
        $this->assertEquals(3, count($messages));
    }

    public static function tearDownAfterClass(){
        //Очистка файла после завершения теста
        file_put_contents(BASE_PATH.DS.'app'.DS.'data'.DS.'channels'.DS.'test-general.json', "");
    }
}
