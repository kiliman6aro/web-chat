<?php
/**
 * User: Bilyk Pavel
 * Date: 17.01.2016
 */

namespace tests\app\components\mappers;

use app\models\User;
use system\core\Registry;

class UsersMapperTest extends \PHPUnit_Framework_TestCase {

    private $_mapper;


    protected function setUp() {
        $this->_mapper = Registry::get('users');
    }

    public function testInsert(){
        $user = User::model(array(
            'username' => strtolower('Kilimangaro'),
            'email' => 'kiliman6aro@gmail.com',
            'password' => "123",
        ));
        $user = $this->_mapper->insert($user);
        $this->assertEquals(1, $user->id);
        $this->assertEquals('kilimangaro', $user->username);
        $this->assertEquals('kiliman6aro@gmail.com', $user->email);
        $this->assertEquals(md5("123"), $user->password);
        $this->assertNull($user->url);

        $user2 = User::model(array(
            'username' => strtolower('Qwerty'),
            'email' => 'kiliman6aro@gmail.com',
            'password' => md5("321"),
            'url' => 'http://google.com'
        ));
        $user2 = $this->_mapper->insert($user2);
        $this->assertEquals(2, $user2->id);
        $this->assertEquals('http://google.com', $user2->url);

        $user3 = new User();
        $user3->username = strtolower("root");
        $user3->password = md5("1234567");
        $user3->email = "root@example.com";

        $user3 = $this->_mapper->insert($user3);
        $this->assertEquals(3, $user3->id);
    }

    public function testFindById(){
        $user = $this->_mapper->findById(3);
        $this->assertEquals(3, $user->id);
        $this->assertEquals('root', $user->username);
    }

    public function testFindByAttributes(){
        $user = $this->_mapper->findByAttributes(array('username' => 'root', 'password' => md5("1234567")));
        $this->assertEquals(3, $user->id);
        $this->assertEquals('root', $user->username);

        //Проверка, что передача неверных параметров, не приведет к исключению
        $user = $this->_mapper->findByAttributes(array('username' => 'unknown'));
        $this->assertNull($user);
    }

    public function testFindAllByAttributes(){
        $users = $this->_mapper->findAllByAttributes(array('email' => 'kiliman6aro@gmail.com'));
        $this->assertEquals(2, count($users));

        //Проверка, что передача неверных параметров, не приведет к исключению
        $users = $this->_mapper->findAllByAttributes(array('username' => 'unknown'));
        $this->assertEquals(0, count($users));
    }

    public function testFindAll(){
        $users = $this->_mapper->findAll();
        $this->assertEquals(3, count($users));

        //Считать еще раз, уже считанные данные
        $cache = $this->_mapper->findAll(false);
        $this->assertEquals(3, count($cache));
    }

    public function testUpdate(){
        $user = $this->_mapper->findById(3);
        $user->email = 'root@gmail.com';
        $this->assertTrue($this->_mapper->update($user));

        $user = $this->_mapper->findById(3);
        $this->assertEquals(3, $user->id);
        $this->assertEquals('root@gmail.com', $user->email);
        $this->assertEquals('root', $user->username);
        $this->assertEquals(md5("1234567"), $user->password);
    }

    public function testDelete(){
        $user = $this->_mapper->findById(1);
        $this->assertTrue($this->_mapper->delete($user));
        //Проверка, что после удаления, идентификаторы продолжают
        //правильно присваиваться
        $newUser = User::model(array(
            'username' => 'alex',
            'email' => 'alex@mail.ru',
            'password' => 'alex'
        ));
        $user = $this->_mapper->insert($newUser);
        $this->assertEquals(4, $user->id);
    }

    public static function tearDownAfterClass(){
        //Очистка файла после завершения теста
        file_put_contents(BASE_PATH.DS.'app'.DS.'data'.DS.'test-users.json', "");
    }


}
