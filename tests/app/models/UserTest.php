<?php
/**
 * User: Bilyk Pavel
 * Date: 19.01.2016
 */

namespace tests\app\models;


use app\models\User;

class UserTest extends \PHPUnit_Framework_TestCase {

    public function testValidate(){
        $user = User::model(array(
            'username' => 'qwerty',
            'password' => '123',
            'email' => 'user@mail.com'
        ));
        $this->assertTrue($user->validate());

        $user = new User();
        $user->username = 'qwerty';
        $this->assertFalse($user->validate());

        $user = new User();
        $user->password = md5('qwerty');
        $this->assertFalse($user->validate());

        $user = new User();
        $user->username = 'qwerty';
        $user->password = md5("root");
        $this->assertTrue($user->validate());

        $user = new User();
        $user->username = 'qwerty';
        $user->password = md5('toor');
        $this->assertFalse($user->validate('registration'));

        $user = new User();
        $user->username = 'qwerty';
        $user->password = md5('toor');
        $user->url = 'domain.com';
        $this->assertFalse($user->validate('registration'));

        $this->assertEquals(2, count($user->getErrors()));

        $user = new User();
        $user->username = 'qwerty';
        $user->email = 'qwerty@exaple.com';
        $user->password = md5('toor');
        $user->url = 'http://example.com';
        $this->assertTrue($user->validate('registration'));
    }
}
