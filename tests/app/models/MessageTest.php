<?php
/**
 * User: Bilyk Pavel
 * Date: 20.01.2016
 */

namespace tests\app\models;


use app\models\Message;

class MessageTest extends \PHPUnit_Framework_TestCase {

    public function testValidate(){
        $msg = new Message("kilimangaro", "Example message for testing!");
        $this->assertTrue($msg->validate());

        $msg = Message::model(array(
            'from' => 'kilimangaro',
            'body' => 'Example message for testing!'
        ));
        $this->assertTrue($msg->validate());

        $msg = new Message("kilimangaro", "Example message for testing!");
        $msg->type = 'unknown';
        $this->assertFalse($msg->validate());
        $this->assertEquals(1, count($msg->getErrors()));

        $msg = new Message("", "Example message for testing!");
        $msg->type = 'unknown';
        $this->assertFalse($msg->validate());
        $this->assertEquals(2, count($msg->getErrors()));

        $msg = new Message("kilimangaro", "");
        $msg->time = 'unknown';
        $this->assertFalse($msg->validate());
        $this->assertEquals(2, count($msg->getErrors()));
    }
}
