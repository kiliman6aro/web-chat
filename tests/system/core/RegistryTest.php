<?php
/**
 * Created by PhpStorm.
 * User: kilim
 * Date: 20.01.2016
 * Time: 11:47
 */

namespace system\core;


class RegistryTest extends \PHPUnit_Framework_TestCase{

    private $_registry;

    public function setUp(){
        $config = require_once(BASE_PATH.DS.'app'.DS.'config'.DS.'test.php');
        $this->_registry = Registry::load($config);
    }

    public function testLoad(){

        $registry = Registry::load();
        $this->assertInstanceOf("\\system\\core\\Registry", $registry);

        $r = Registry::load('');
        $this->assertInstanceOf("\\system\\core\\Registry", $r);
    }

    public function testGet(){
        $this->assertNotNull(Registry::get('users'));
        $this->assertInstanceOf("\\app\\components\\mappers\\UsersMapper", Registry::get('users'));
    }

    /**
     * @expectedException \Exception
     */
    public function testGetUnknown(){
        Registry::get("unknown");
    }

    public function testSet(){
        $obj = new \stdClass();
        Registry::set("example", $obj);
        $this->assertInstanceOf("\\stdClass", Registry::get("example"));
    }
}
