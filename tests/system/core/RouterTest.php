<?php
/**
 * User: Bilyk Pavel
 * Date: 17.01.2016
 */

namespace tests\system\core;

use system\core\Router;

class RouterTest extends \PHPUnit_Framework_TestCase {
    private $_router;

    public function setUp(){
        $this->_router = new Router("main/index");
    }

    public function testGetControllerID(){
        $this->assertEquals("main", $this->_router->getControllerID());
    }

    public  function  testGetActionID(){
        $this->assertEquals("index", $this->_router->getActionID());
    }

    public function  testGetActionName(){
        $this->assertEquals("actionIndex", $this->_router->getActionName());
    }
    public function testGetControllerName(){
        $this->assertEquals("\\app\\controllers\\MainController", $this->_router->getControllerName());
    }

    public function testStart(){
        $this->assertNotFalse($this->_router->start());
    }
    /**
     * @expectedException \Exception
     */
    public function testStartWithException(){
        $this->_router = new Router("default/view");
        $this->_router->start();
    }

    /**
     * @expectedException \Exception
     */
    public function testLoad(){
        Router::load("main/view");
    }
}
