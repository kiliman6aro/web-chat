<?php
/**
 * User: Bilyk Pavel
 * Date: 19.01.2016
 */

define('DEBUG_MODE', true);

$bootstrap = require_once(dirname(__FILE__).'/../system/bootstrap.php');
$config = require_once(dirname(__FILE__).'/../app/config/test.php');

spl_autoload_unregister('autoload');

spl_autoload_register('spl_autoload');

use \system\core\Registry;
Registry::load($config);


