<?php
/**
 * Created by PhpStorm.
 * User: kilim
 * Date: 20.01.2016
 * Time: 10:50
 */

namespace system\core;


class Registry{

    private static $_instance;

    private $_stack = array();


    public static function load($config = null){
        if(!self::$_instance || is_array($config))
            self::$_instance = new Registry($config);

        return self::$_instance;
    }

    private function __construct($config = array()){
        if(empty($config))
            return;

        if(!isset($config['components']))
            return;

        $this->_stack = array(); //Обнуляю стек
        foreach ($config['components'] as $name => $params) {
            if(!isset($params['class']))
                continue;

            $component = new $params['class']();
            unset($params['class']);

            foreach ($params as $key => $value) {
                if(!property_exists($component, $key))
                    continue;

                $component->$key = $value;
            }
            $component->init();
            $this->_stack[$name] = $component;
        }
    }

    public function __get($name){
        if(isset($this->_stack[$name]))
            return $this->_stack[$name];

        throw new \Exception("Не найден компонент $name");
    }

    public function __set($name, $component){
        $this->_stack[$name] = $component;
    }

    public static function get($name){
        return Registry::load()->$name;
    }

    public static function set($name, $component){
        return Registry::load()->$name = $component;
    }
}