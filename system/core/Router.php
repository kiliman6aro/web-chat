<?php
namespace system\core;


class Router {

    const DEFAULT_ACTION = 'index';

    const DEFAULT_CONTROLLER = 'main';

    private $_url;

    private $_controllerID;

    private $_actionID;

    private $_query = array();

    public function __construct($url){
        $this->_url = $url;
        $arrayUrl = explode('/', $url);
        $controller = array_shift($arrayUrl);
        $action = array_shift($arrayUrl);
        $this->_controllerID = !empty($controller) ? $controller: self::DEFAULT_CONTROLLER;
        $this->_actionID = !empty($action) ? $action : self::DEFAULT_ACTION;
        $this->_query = $arrayUrl;
    }

    /**
     * Инициализация контроллера и вызов
     * у его action.
     */
    public function start(){
        $class = $this->getControllerName();
        if(!class_exists($class)){
            throw new \Exception("Контроллер ".$this->getControllerName()." не найден");
        }
        $controller = new $class($this->_controllerID);
        $controller->init();
        if(method_exists($controller, $this->getActionName())){
            return call_user_func_array(array($controller,$this->getActionName()),$this->_query);
        }else{
            throw new \Exception("Метод ".$this->getActionName().' в контроллере '.$class. 'не найден!');
        }
    }

    public function getActionID(){
        return $this->_actionID;
    }

    public function getControllerID(){
        return $this->_controllerID;
    }

    public function getActionName(){
        return 'action'.ucfirst($this->_actionID);
    }
    public function getControllerName(){
        return '\\app\\controllers\\'.ucfirst($this->_controllerID).'Controller';
    }

    /**
     * Короткий способ постороения и выполнения запроса
     */
    public static function load($url){
        $router = new Router($url);
        return $router->start();
    }
}