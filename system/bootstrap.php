<?php
define('DS', DIRECTORY_SEPARATOR);

define('BASE_PATH', dirname(__FILE__).DS.'..');

require_once(BASE_PATH.DS.'system'.DS.'core'.DS.'interfaces.php');

spl_autoload_register('autoload');


function autoload($namespace){
    $path = str_replace('\\', DS, $namespace);
    $file = BASE_PATH.DS."{$path}.php";
    if(!file_exists($file)){
        include(BASE_PATH.DS.'public'.DS.'500.php');
    }
    require_once $file;
}

if (DEBUG_MODE == true) {
    ini_set('display_errors','On');
} else {
    ini_set('display_errors','Off');
    ini_set('log_errors', 'On');
    ini_set('error_log', BASE_PATH.DS.'tmp'.DS.'logs'.DS.'error.log');
}
error_reporting(E_ERROR | E_WARNING);