<?php 

namespace system\web;

use Exception;

abstract class BaseController {
    
    
    protected $layout = 'main';
    
    protected $id;

    public function __construct($id, $layout = 'main'){
        $this->id = $id;
        $this->layout = $layout;
    }
    public function init(){

    }
    /**
     * Генерирует HTML и выводит на экран
     */
    public function display($name, $params = array()){
        $file = $this->getViewsPath().DS.$name.'.php';
        if(file_exists($file)){
            $content = $this->render($name, $params);
            $file = $this->getLayoutsPath().DS.$this->layout.'.php';
            $this->renderFile($file, array('content' => $content));
        }else{
            throw new Exception("Не найден файл отображения ".$name);
        }
    }

    /**
     * Генерирует файл и возвращает результат (без вывода на экран)
     * @param $name
     * @param array $params
     * @return string
     * @throws Exception
     */
    public function render($name, $params = array()){
        $file = $this->getViewsPath().DS.$name.'.php';
        if(file_exists($file)){
            return $this->renderFile($file, $params, true);
        }else{
            throw new Exception("Не найден файл отображения ".$file);
        }
    }
    
    /**
     * Подключает файл и буферизует если это необходимо
     */
    public function renderFile($file, $data = array(), $return = false){
        extract($data);
        if($return){
            ob_start();
            ob_implicit_flush(false);
            require($file);
            return ob_get_clean();
        }
        require($file);
    }
    
    protected function getViewsPath(){
        return BASE_PATH.DS.'app'.DS.'views'.DS.$this->id;
    }
    
    protected function getLayoutsPath(){
        return BASE_PATH.DS.'app'.DS.'views'.DS.'layouts';
    }
}